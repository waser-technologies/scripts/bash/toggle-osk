# Toggle On-Screen Keyboard

No more on-screen keyboard if you use a keyboard.

Works with mutliple keyboards:
	-	USB Keyboards
	-	Bluetooth Keyboards
	-	Pinephone Keyboard Case
	-	Many more

### Installation

Get all the files and install them to your system. If your userid is not 'manjaro' use the homedir of your user. 

The layout looks like this:

/etc/udev/rules.d/99-external-keyboard.rules

/etc/udev/tgkb -> symlink to /usr/local/bin/tgkb

/home/manjaro/.local/share/applications/switch_keyboard.desktop

/home/manjaro/.config/systemd/user/tgkb.service

/usr/local/bin/tgkb

### Test using an external keyboard

Open a program that forces the on-screen keyboard to show (e.g. King's Cross or another terminal).

Connect either an usb or a bluetooth keyboard. The on-screen keyboard should disappear.

Some helpful command for debugging:

`sudo udevadm monitor -s input -t seat` monitor udev events
`sudo udevadm info /sys{device-path}` show information about an device

### Test using the 'app' "Toggle Keyboard"

Tap on the icon "switch keyboard" in your app drawer. The icon shows a keyboard. This toggles the setting for showing the on-screen keyboard.

### Using tgkb from terminal

The script can be called as root or as the user running gnome in a terminal:

    waser@manjaro:~$ tgkb -h
    Usage:
     tgkb [options}
     
     Switches automatic showing of the screen keyboard on or off or shows the current status.
    
     If called without parameters will readout the screen keyboard setting and return current status to stdout.
    
     Keyboard behaviour:
     If ommited will look at the current status and return it to chosen message output.
     Using more than one option will result in respecting the last option.
     -k	show os keyboard
     -n	do not show screen keyboard
     -s	togggle between os keyboard enabled and os keyboard disabled
     -S Check if PinePhone Keyboard case is connected over i2c to determine wheather to show OSK or not
    
     General:
     -h	Show this message

### Deinstallation

Delete the files from your system and run `sudo udevadm -R` again.

### Hardware known to work

Tested on a pinephone running mobian using the following hardware:

* LG Rolly Bluetooth keyboard (when disconnecting give it some time to let udev catch up and the on-screen keyboard re-appear)
* Enermax usb wireless keyboard
* Yubikey ;-)
* PinePhone Keyboard Case

### Contribution

Many thanks to [ChriChri's Proof-of-Concept](https://codeberg.org/ChriChri/switch_keyboard)!
